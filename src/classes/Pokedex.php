<?php
class Pokedex {
    public function apiGet($pokemon = null) {
        global $message_array;
        $pokeUrl = "https://pokeapi.co/api/v2/pokemon/" . strtolower($pokemon);
        $pokeApiGet = @file_get_contents($pokeUrl);
        $pokeArray = json_decode($pokeApiGet, true);
        if ($pokemon != null) {
            if (is_array($pokeArray) and array_search(strtolower($pokemon), $pokeArray)) {
                return $pokeArray;                
            } else {
                return false;
            }
        } else {
            return $pokeArray;
        }
    }

}

$Pokedex = new Pokedex;