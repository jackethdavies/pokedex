<?php
// Main class to handle the application
require_once("../src/classes/Pokedex.php");    

// Stop the unidentified index errors
if (isset($_GET['search'])) {
    $getSearched = $_GET['search'];            
    $getSearchedPokemon = $Pokedex->apiGet($getSearched);
} else {
    $getSearched = NULL;
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <link rel="stylesheet" href="../src/assets/style/main.css">
</head>
<body>
    <header>
        <div class="wrapper">
            <h1>Pokedex</h1>
            <form method="get">
                <div class="input-group">
                    <input type="text" class="input" name="search" placeholder="Name or ID...">
                    <input type="submit" class="input" value="Search">
                </div>
            </form>
        </div>
    </header>

    <section class="app">
    <?php if (isset($getSearched) and $Pokedex->apiGet($getSearched)) { ?>
        <div class="poke-stats">
            <div class="container">
                <h1><?= ucfirst($getSearchedPokemon['name']); ?></h1>
                <ul>
                    <?php if ($getSearchedPokemon['sprites']['front_default']) { ?>
                        <li class="poke-item image"><img src="<?= $getSearchedPokemon['sprites']['front_default']; ?>" alt="Pokemon photo"></li>
                    <?php } ?>
                    <li class="poke-item">Name: <?= ucfirst($getSearchedPokemon['name']); ?></li>
                    <li class="poke-item">Species: <?= ucfirst($getSearchedPokemon['species']['name']); ?></li>
                    <li class="poke-item">Height: <?= $getSearchedPokemon['height']; ?>m / Weight: <?= $getSearchedPokemon['weight']; ?>kg</li>
                    <li class="poke-item red">Abilities</li>
                    <?php
                        foreach ($getSearchedPokemon['abilities'] as $getAbilities) {
                            if ($getAbilities['is_hidden'] == 0) {
                                echo "<li class='poke-item'>".ucfirst($getAbilities['ability']['name'])."</li>";
                            }
                        }
                        echo "<li class='poke-item red'>Statistics</li>";
                        foreach ($getSearchedPokemon['stats'] as $getStats) {
                            $formatStats = $getStats['stat']['name'];
                            switch($formatStats) {
                                case "hp":
                                    $stat = "HP";
                                break;
                                case "special-defense":
                                    $stat = "Special Defence";
                                break;
                                case "special-attack":
                                    $stat = "Special Attack";
                                break;
                                default:
                                    $stat = $formatStats;
                                break;
                            }
                            echo "<li class='poke-item'>". ucfirst($stat) .": <strong>". $getStats['base_stat'] ."</strong></li>";
                            // echo "<li class='poke-item'>".  ."</li>";
                            
                        }
                    ?>
                </ul>    
            </div>
        </div>
    <?php } else { ?>
        <div class="poke-stats">
            <div class="container">
                <h1>Notice</h1>
                <ul>
                    <li class="poke-item">Search for a Pokemon or click on one!</li>
                </ul>
            </div>
        </div>
    <?php } ?>
        <div class="poke-list">
            <div class="container">
                <h1>Pokemon List</h1>
                <ul class='main-list'>
                <?php
                    $pdex = $Pokedex->apiGet();
                    foreach($pdex['results'] as $pokemon) {
                        if (strtolower($getSearched) == $pokemon['name']) {
                            echo "<li class='poke-item active'><a href='?search=". $pokemon['name'] ."'>". ucfirst($pokemon['name']) . "</a></li>";
                        } else {
                            echo "<li class='poke-item'><a href='?search=". $pokemon['name'] ."'>". ucfirst($pokemon['name']) . "</a></li>";
                        }
                    } ?>

                </ul>
            </div>
        </div>

    </section>
</body>
</html>